//
// Created by mokovalevskyi on 12/28/20.
//

#include "Person.h"
#include <iostream>

User::User(int id, std::string name) : m_id(id), m_name(std::move(name))
{}

User::User(const User &other)
{
    m_id = other.m_id;
    m_name = other.m_name;
}

void User::set_id(int id)
{
    m_id = id;
}

int User::get_id() const
{
    return m_id;
}

void User::set_name(std::string &name)
{
    m_name = name;
}
std::string User::get_name() const
{
    return m_name;
}

User &User::operator=(const User &other)
{
    m_id = other.m_id;
    m_name = other.m_name;
}

Person::Person(int priority, int id, std::string name, std::string number)
        : User(id, name), m_priority(priority), m_number(std::move(number)) {}

Person::Person(const Person &person)
        : Person(person.m_priority, person.m_id, person.m_name, person.m_number)
{}

int Person::get_priority() const
{
    return m_priority;
}

std::string Person::get_number() const
{
    return m_number;
}

Person &Person::operator=(const Person &person)
{
    User::operator=(static_cast<User>(person));
    m_priority = person.m_priority;
    m_number = person.m_number;
}

void Person::set_priority(int priority)
{
    m_priority = priority;
}

void Person::set_number(std::string &number)
{
    m_number = number;
}

VipPerson::VipPerson(int priority, int id, std::string name, std::string number, std::string cardNumber)
        : Person(priority, id, std::move(name), std::move(number)), m_cardNumber(std::move(cardNumber)) {}

VipPerson::VipPerson(const VipPerson & vipPerson)
        : Person(vipPerson), m_cardNumber(vipPerson.m_cardNumber)
{}

VipPerson &VipPerson::operator=(VipPerson && vipPerson)
{
    Person::operator=(static_cast<Person>(vipPerson));
    m_cardNumber = vipPerson.m_cardNumber;
}

void VipPerson::set_card_number(std::string &cardNumber)
{
    m_cardNumber = cardNumber;
}

std::string VipPerson::get_card_number() const
{
    return m_cardNumber;
}

std::ostream& operator<<(std::ostream &os, const User &user)
{
    os << "ID: " << user.get_id() << std::endl;
    os << "Name: " << user.get_name() << std::endl;
    return os;
}

std::ostream& operator<<(std::ostream &os, const Person &person)
{
    os << "Priority: " << person.get_priority() << std::endl;
    os << static_cast<User>(person);
    os << "Number: " << person.get_number() << std::endl;
    return os;
}

std::ostream& operator<<(std::ostream &os, const VipPerson &vipPerson)
{
    os << static_cast<Person>(vipPerson);
    os << "Card number: " << vipPerson.get_card_number() << std::endl;
    return os;
}
