//
// Created by mokovalevskyi on 12/28/20.
//

#ifndef PRACTICE_PERSON_H
#define PRACTICE_PERSON_H
#include <string>

class User
{
public:
    User() = default;
    User(int id, std::string name);
    User(const User &other);
    void set_id(int id);
    int get_id() const;
    void set_name(std::string &name);
    std::string get_name() const;
    User &operator=(const User &other);
    friend std::ostream& operator<<(std::ostream &os, const User &user);
    virtual ~User() = default;
protected:
    int m_id;
    std::string m_name;
};

class Person : public User
{
public:
    Person() = default;
    Person(int priority, int id, std::string name, std::string number);
    Person(const Person &person);
    void set_priority(int priority);
    int get_priority() const;
    void set_number(std::string &number);
    std::string get_number() const;
    Person &operator=(const Person &person);
    friend std::ostream& operator<<(std::ostream &os, const Person &person);
    virtual ~Person() = default;
protected:
    int m_priority;
    std::string m_number;
};

class VipPerson : public Person {
public:
    VipPerson() = default;
    VipPerson(int priority, int id, std::string name, std::string number, std::string cardNumber);
    VipPerson(const VipPerson & vipPerson);
    VipPerson &operator=(VipPerson && vipPerson);
    void set_card_number(std::string &cardNumber);
    std::string get_card_number() const;
    friend std::ostream& operator<<(std::ostream &os, const VipPerson &vipPerson);
    ~VipPerson() = default;
private:
    std::string m_cardNumber;
};

#endif //PRACTICE_PERSON_H
