#pragma once
#include <memory>
#include "shared_counter.h"

namespace memory
{
    template <typename T> class shared_ptr;
    template <typename T> class weak_ptr;

    template<typename T>
    class weak_ptr {
    public:
        template<class Y> friend class shared_ptr;
        template<class Y> friend class weak_ptr;
        constexpr weak_ptr() noexcept
        {
            m_obj = nullptr;
            m_count = nullptr;
        }

        weak_ptr(const weak_ptr &r) noexcept
                : weak_ptr()
        {
            m_count = r.m_count;
            m_obj = r.m_obj;
        }

        template<class Y>
        weak_ptr(const weak_ptr<Y> &r) noexcept
                : weak_ptr()
        {
            m_count = r.m_count;
            m_obj = dynamic_cast<T *>(r.m_obj);
        }

        weak_ptr(weak_ptr &&r) noexcept
                : weak_ptr()
        {
            m_count = r.m_count;
            m_obj = r.m_obj;
            r.m_obj = nullptr;
            r.m_count = nullptr;
        }

        template<class Y>
        weak_ptr(weak_ptr<Y> &&r) noexcept
                : weak_ptr()
        {
            m_count = r.m_count;
            m_obj = dynamic_cast<T *>(r.m_obj);
            r.m_obj = nullptr;
            r.m_count = nullptr;
        }

        template<class Y>
        weak_ptr(const shared_ptr<Y> &r) noexcept
                : weak_ptr()
        {
            m_obj = r.get();
            m_count = r.m_count;
        }

        weak_ptr &operator=(const weak_ptr &r) noexcept
        {
            m_count = r.m_count;
            m_obj = r.m_obj;
        }

        template<class Y>
        weak_ptr &operator=(const weak_ptr<Y> &r) noexcept
        {
            m_obj = dynamic_cast<T *>(r.m_obj);
            m_count = r.m_count;
        }

        weak_ptr &operator=(weak_ptr &&r) noexcept
        {
            m_count = r.m_count;
            m_obj = r.m_obj;
            r.m_obj = nullptr;
            r.m_count = nullptr;
        }

        template<class Y>
        weak_ptr &operator=(weak_ptr<Y> &&r) noexcept
        {
            m_count = r.m_count;
            m_obj = dynamic_cast<T *>(r.m_obj);
            r.m_obj = nullptr;
            r.m_count = nullptr;
        }

        template<class Y>
        weak_ptr &operator=(const shared_ptr<Y> &r) noexcept
        {
            m_obj = dynamic_cast<T*>(r.m_obj);
            m_count = r.m_count;
        }

        shared_ptr<T> lock() const noexcept
        {
            return expired() ? shared_ptr<T>() : shared_ptr<T>(*this);
        }

        void reset() noexcept
        {
            m_obj = nullptr;
            m_count = nullptr;
        }

        void swap(weak_ptr& r) noexcept
        {
            std::swap(*this, r);
        }

        unsigned long use_count() const
        {
            if (!m_count) {
                return 0;
            }

            return m_count->use_count();
        }

        bool expired() const noexcept
        {
            return !use_count();
        }

        ~weak_ptr() = default;

    protected:
        T * m_obj;
        shared_counter * m_count;
    };


    template<typename T>
    class shared_ptr
    {
        template<class Y> friend class weak_ptr;
        template<class Y> friend class shared_ptr;
    protected:
        T * m_obj;
        shared_counter * m_count;
        using element_type = T;

        template<class Deleter, class Allocator, class... Args>
        shared_ptr(Deleter deleter, Allocator allocator, Args&&... args)
            : m_obj(nullptr), m_count(nullptr)
        {
            m_count = new shared_counter(allocator, std::forward<Args>(args)...);
            m_obj = static_cast<T*>(m_count->get_pointer());
            ++(*m_count);
        }
    public:

        constexpr shared_ptr() noexcept
            : m_count(nullptr), m_obj(nullptr)
        {}

        constexpr shared_ptr(std::nullptr_t ptr)
            : m_count(nullptr), m_obj(ptr)
        {}

        template<class Y>
        explicit shared_ptr(Y * ptr)
            : m_count(nullptr), m_obj(nullptr)
        {
            if constexpr (std::is_integral<Y>::value || std::is_floating_point<Y>::value) {
                m_obj = static_cast<T *>(ptr);
            } else {
                m_obj = dynamic_cast<T *>(ptr);
            }

            m_count = new shared_counter(m_obj);
            ++(*m_count);
        }

        template<class Y, class Deleter>
        shared_ptr(Y * ptr, Deleter d)
            : m_count(nullptr), m_obj(nullptr)
        {
            if constexpr (std::is_integral<Y>::value || std::is_floating_point<Y>::value) {
                m_obj = static_cast<T *>(ptr);
            } else {
                m_obj = dynamic_cast<T *>(ptr);
            }

            m_count = new shared_counter(m_obj, d);
            ++(*m_count);
        }

        template<class Y, class Deleter, class Allocator>
        shared_ptr(Y * ptr, Deleter d, Allocator a)
            : m_count(nullptr), m_obj(nullptr)
        {
            if constexpr (std::is_integral<Y>::value || std::is_floating_point<Y>::value) {
                m_obj = static_cast<T *>(ptr);
            } else {
                m_obj = dynamic_cast<T *>(ptr);
            }

            m_count = new shared_counter(m_obj, d, a);
            ++(*m_count);
        }

        template<class Deleter, class Allocator>
        shared_ptr(std::nullptr_t ptr, Deleter d, Allocator a)
            :  m_count(nullptr), m_obj(nullptr)
        {
            m_obj = static_cast<T*>(ptr);
            m_count = new shared_counter(m_obj, d, a);
        }

        shared_ptr(const shared_ptr& r) noexcept
            : m_count(r.m_count), m_obj(r.m_obj)
        {
            if (m_obj) {
                ++(*m_count);
            }
        }

        shared_ptr(shared_ptr&& r) noexcept
            : m_count(r.m_count), m_obj(r.m_obj)
        {
            r.m_obj = nullptr;
            r.m_count = nullptr;
        }

        template<class Y>
        shared_ptr(const shared_ptr<Y>& r) noexcept
            : m_count(r.m_count), m_obj(nullptr)
        {
            if constexpr (std::is_integral<Y>::value || std::is_floating_point<Y>::value) {
                m_obj = static_cast<T *>(r.m_obj);
            } else {
                m_obj = dynamic_cast<T *>(r.m_obj);
            }

            if (m_obj) {
                ++(*m_count);
            }
        }

        template<class Y>
        shared_ptr(shared_ptr<Y>&& r) noexcept
            : m_count(r.m_count), m_obj(nullptr)
        {
            if constexpr (std::is_integral<Y>::value || std::is_floating_point<Y>::value) {
                m_obj = static_cast<T *>(r.m_obj);
            } else {
                m_obj = dynamic_cast<T *>(r.m_obj);
            }

            r.m_count = nullptr;
            r.m_obj = nullptr;
        }

        template<class Y>
        explicit shared_ptr(const weak_ptr<Y> &r)
            : m_count(nullptr), m_obj(nullptr)
        {
            if (r.m_obj) {
                if constexpr (std::is_integral<Y>::value || std::is_floating_point<Y>::value) {
                    m_obj = static_cast<T *>(r.m_obj);
                } else {
                    m_obj = dynamic_cast<T *>(r.m_obj);
                }
            } else {
                m_obj = r.m_obj;
            }

            m_count = r.m_count;

            if (m_count) {
                ++(*m_count);
            }
        }

        template<class Y, class Deleter>
        shared_ptr(std::unique_ptr<Y, Deleter>&& r)
            : m_count(nullptr), m_obj(dynamic_cast<T*>(r.release()))
        {
            m_count = new shared_counter(m_obj);
            ++(*m_count);
        }

        template< class Y >
        shared_ptr( const shared_ptr<Y>& r, element_type* ptr ) noexcept
        {
            if constexpr (std::is_integral<Y>::value || std::is_floating_point<Y>::value) {
                m_obj = static_cast<T *>(ptr);
            } else {
                m_obj = dynamic_cast<T *>(ptr);
            }

            m_count = r.m_count;
            ++(*m_count);
        }

        explicit operator bool() const noexcept
        {
            return get() != nullptr;
        }

        shared_ptr& operator=(const shared_ptr& r) noexcept
        {
            if (m_obj)
            {
                --(*m_count);
                if (!bool(*m_count)) {
                    delete m_count;
                }
            }

            m_count = r.m_count;
            m_obj = r.m_obj;
            ++(*m_count);
            return *this;
        }

        template<class Y>
        shared_ptr& operator=(const shared_ptr<Y> &r) noexcept
        {
            if (m_obj) {
                --(*m_count);
                if (!bool(*m_count)) {
                    delete m_count;
                }
            }

            m_count = r.m_count;
            m_obj = dynamic_cast<T*>(r.m_obj);
            ++(*m_count);
            return *this;
        }

        shared_ptr& operator=(shared_ptr&& r) noexcept
        {
            if (m_count) {
                --(*m_count);
                if (m_obj) {
                    if (!bool(*m_count)) {
                        delete m_count;
                    }
                }
            }

            m_count = r.m_count;
            m_obj = r.m_obj;
            r.m_count = nullptr;
            r.m_obj = nullptr;
            return *this;
        }

        template<class Y>
        shared_ptr& operator=(shared_ptr<Y>&& r) noexcept
        {
            if (m_count) {
                --(*m_count);
                if (m_obj) {
                    if (!bool(*m_count)) {
                        delete m_count;
                    }
                }
            }

            m_count = r.m_count;
            m_obj = dynamic_cast<T*>(r.m_obj);
            r.m_count = nullptr;
            r.m_obj = nullptr;
            return *this;
        }

        template<class Y, class Deleter>
        shared_ptr& operator=(std::unique_ptr<Y,Deleter>&& r)
        {
            shared_ptr<T>(std::move(r)).swap(*this);
        }

        int use_count() const noexcept
        {
            if (!m_count) {
                return 0;
            }

            return m_count->use_count();
        }

        bool unique() const noexcept
        {
            return m_count->unique();
        }

        void reset() noexcept
        {
            shared_ptr().swap(*this);
        }

        template<class Y>
        bool owner_before( const shared_ptr<Y>& other) const noexcept
        {
            return m_count->owner_before(*other.m_count);
        }

        template<class Y>
        bool owner_before( const weak_ptr<Y>& other) const noexcept
        {
            return m_count->owner_before(*other.m_count);
        }

        void reset(T *obj)
        {
            if (!m_obj && !m_count) {
                m_obj = obj;
                m_count = new shared_counter(m_obj);
                ++(*m_count);
                return;
            }

            if (!m_obj) {
                m_obj = obj;
                ++(*m_count);
                return;
            }

            if (m_obj && use_count() > 1) {
                m_obj = obj;
                --(*m_count);
                m_count = new shared_counter(m_obj);
                ++(*m_count);
            }

            if (use_count() < 2 && m_obj) {
                delete m_count;
                m_obj = obj;
                m_count = new shared_counter(m_obj);
                ++(*m_count);
            }
        }

        T &operator*() const noexcept
        {
            return *get();
        }

        T *operator->() const noexcept
        {
            return get();
        }

        T *get() const noexcept
        {
            return m_obj;
        }

        void swap(shared_ptr<T> &ptr) noexcept
        {
            std::swap(m_count, ptr.m_count);
            std::swap(m_obj, ptr.m_obj);
        }

        template <class Y, class Alloc, class... Args>
        friend shared_ptr<Y> allocate_shared(const Alloc &alloc, Args&&... args);

        template<typename Deleter, typename Y>
        friend Deleter get_deleter(const shared_ptr<Y> &p) noexcept;

        ~shared_ptr()
        {
            if (m_count) {
                --(*m_count);
                if (!bool(*m_count)) {
                    delete m_count;
                }
            }

            m_count = nullptr;
            m_obj = nullptr;
        }
    };

    template <class Y, class Alloc, class... Args>
    inline shared_ptr<Y> allocate_shared(const Alloc &alloc, Args&&... args)
    {
        return shared_ptr<Y>(std::default_delete<Y>(), alloc, std::forward<Args>(args)...);
    }

    template<typename Y, class ...Args>
    inline shared_ptr<Y> make_shared(Args&&... args)
    {
        typedef typename std::remove_const<Y>::type Y_nc;
        return memory::allocate_shared<Y>(std::allocator<Y_nc>(), std::forward<Args>(args)...);
    }

    template<typename Y>
    shared_ptr<Y> make_shared(const size_t size)
    {
        Y * arr = new Y[size];
        return shared_ptr<Y>(arr, std::default_delete<Y[]>(), std::allocator<Y>());
    }

    template<typename Deleter, typename Y>
    Deleter get_deleter(const shared_ptr<Y> &p) noexcept
    {
        Deleter * pDeleter = static_cast<Deleter*>(p.m_count->get_deleter());
        return static_cast<Deleter>(*pDeleter);
    }

    template<class T, class U, class V>
    std::basic_ostream<U,V>& operator<<(std::basic_ostream<U,V>& os, const shared_ptr<T> &ptr)
    {
        os << ptr.get();
        return os;
    }

    template <class T, class U>
    inline bool operator==(const shared_ptr<T> &lhs, const shared_ptr<U> &rhs) noexcept
    {
        return lhs.get() == rhs.get();
    }

    template <class T, class U>
    inline bool operator!=(const shared_ptr<T> &lhs, const shared_ptr<U> &rhs) noexcept
    {
        return !(lhs == rhs);
    }

    template<class T, class U>
    inline bool operator<(const shared_ptr<T> &lhs, const shared_ptr<U> &rhs) noexcept
    {
        return lhs.get() < rhs.get();
    }

    template<class T, class U>
    inline bool operator>(const shared_ptr<T> &lhs, const shared_ptr<U> &rhs) noexcept
    {
        return lhs.get() > rhs.get();
    }

    template<class T, class U>
    bool operator<=(const shared_ptr<T> &lhs, const shared_ptr<U> &rhs) noexcept
    {
        bool less = lhs < rhs;
        bool equal = lhs == rhs;
        return less || equal;
    }

    template<class T, class U>
    bool operator>=(const shared_ptr<T> &lhs, const shared_ptr<U> &rhs) noexcept
    {
        bool less = rhs < lhs;
        bool equal = rhs == lhs;
        return less || equal;
    }

    template <class T>
    inline bool operator==(const shared_ptr<T>& lhs, std::nullptr_t)
    {
        return !bool(lhs);
    }

    template <class T>
    inline bool operator==(std::nullptr_t, const shared_ptr<T>& rhs)
    {
        return !bool(rhs);
    }

    template <class T>
    inline bool operator!=(const shared_ptr<T>& lhs, std::nullptr_t)
    {
        return lhs.get() != nullptr;
    }

    template <class T>
    bool operator!=(std::nullptr_t, const shared_ptr<T>& rhs)
    {
        return rhs != nullptr;
    }

    template <class T>
    inline bool operator<(const shared_ptr<T>& lhs, std::nullptr_t)
    {
        return lhs.get() < nullptr;
    }

    template <class T>
    bool operator<(std::nullptr_t, const shared_ptr<T>& rhs)
    {
        return !(rhs < nullptr);
    }

    template <class T>
    bool operator>(const shared_ptr<T>& lhs, std::nullptr_t)
    {
        return nullptr < lhs;
    }

    template <class T>
    bool operator>(std::nullptr_t, const shared_ptr<T>& rhs)
    {
        return rhs < nullptr;
    }

    template <class T>
    bool operator<=(const shared_ptr<T>& lhs, std::nullptr_t)
    {
        bool less = lhs < nullptr;
        bool equal = lhs == nullptr;
        return less || equal;
    }

    template <class T>
    bool operator<=(std::nullptr_t, const shared_ptr<T>& rhs)
    {
        bool less = nullptr < rhs;
        bool equal = nullptr == rhs;
        return less || equal;
    }

    template <class T>
    bool operator>=(const shared_ptr<T>& lhs, std::nullptr_t)
    {
        bool less = lhs > nullptr;
        bool equal = lhs == nullptr;
        return less || equal;
    }

    template <class T>
    bool operator>=(std::nullptr_t, const shared_ptr<T>& rhs)
    {
        bool less = nullptr > rhs;
        bool equal = nullptr == rhs;
        return less || equal;
    }

    template <class T, class U>
    shared_ptr<T> static_pointer_cast(const shared_ptr<U> &r) noexcept
    {
        auto p = static_cast<T*>(r.get());
        return shared_ptr<T>(r, p);
    }

    template <class T, class U>
    shared_ptr<T> dynamic_pointer_cast(const shared_ptr<U> &r) noexcept
    {
        if (auto p = dynamic_cast<T*>(r.get())) {
            return shared_ptr<T>(r, p);
        } else {
            return shared_ptr<T>();
        }
    }

    template <class T, class U>
    shared_ptr<T> const_pointer_cast(const shared_ptr<U> &r) noexcept
    {
        auto p = const_cast<T*>(r.get());
        return shared_ptr<T>(r, p);
    }

    template <class T, class U>
    shared_ptr<T> reinterpret_pointer_cast(const shared_ptr<U> &r) noexcept
    {
        auto p = reinterpret_cast<T*>(r.get());
        return shared_ptr<T>(r, p);
    }
}
