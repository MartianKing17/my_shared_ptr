//
// Created by mokovalevskyi on 1/12/21.
//

#ifndef TEST_SHARED_COUNTER_H
#define TEST_SHARED_COUNTER_H
#include "manager.h"

class shared_counter
{
private:
    typedef unsigned int uint;
    uint m_count;
    uint m_weak_count;
    manager * m_manager;
public:
    shared_counter();
    template <typename T>
    shared_counter(T * obj);
    template <class T, class Deleter>
    shared_counter(T * obj, Deleter deleter);
    template <class T, class Deleter, class Allocator>
    shared_counter(T * obj, Deleter deleter, Allocator allocator);
    template<class Allocator, class... Args>
    shared_counter(Allocator allocator, Args&&... args);
    inline void operator++();
    explicit operator bool() const;
    inline void operator++(int);
    inline void operator--();
    inline void operator--(int);
    inline uint use_count();
    bool owner_before(const shared_counter& other);
    bool unique();
    void * get_deleter();
    void * get_pointer();
    void destroy();
    ~shared_counter();
};

shared_counter::shared_counter()
        : m_count(0), m_weak_count(0), m_manager(nullptr) {}

template <typename T>
shared_counter::shared_counter(T * obj)
        : m_count(0), m_weak_count(0), m_manager(nullptr)
{
    m_manager = new pointer_manager<T>(obj);
}

template <class T, class Deleter>
shared_counter::shared_counter(T * obj, Deleter deleter)
        : m_count(0), m_weak_count(0), m_manager(nullptr)
{
    typedef super_manager<T, Deleter, std::allocator<void>> superman;
    std::allocator<superman> allocator;
    superman * mem = allocator.allocate(1);
    allocator.construct(mem, obj, std::move(deleter), std::allocator<void>());
    m_manager = mem;
    mem = nullptr;
}

template <class T, class Deleter, class Allocator>
shared_counter::shared_counter(T * obj, Deleter deleter, Allocator allocator)
        : m_count(0), m_weak_count(0), m_manager(nullptr)
{
    typedef super_manager<T, Deleter, Allocator> superman;
    std::allocator<superman> alloc;
    superman * mem = alloc.allocate(1);
    allocator.construct(mem, obj, std::move(deleter), std::move(allocator));
    m_manager = mem;
    mem = nullptr;
}

template<class Allocator, class... Args>
shared_counter::shared_counter(Allocator allocator, Args&&... args)
        : m_count(0), m_weak_count(0), m_manager(nullptr)
{
    typedef typename Allocator::value_type value_type;
    typedef allocate_pointer_manager<value_type, Allocator> point_manager;
    typedef std::allocator<point_manager> allocate_type;

    allocate_type alloc;
    point_manager * mem = alloc.allocate(1);
    alloc.construct(mem, allocator, std::forward<Args>(args)...);
    m_manager = mem;
    mem = nullptr;
}

void shared_counter::operator++()
{
    (m_count)++;
}

shared_counter::operator bool() const
{
    return m_count;
}

void shared_counter::operator++(int)
{
    (m_count)++;
}

void shared_counter::operator--()
{
    (m_count)--;
}

void shared_counter::operator--(int)
{
    (m_count)--;
}

uint shared_counter::use_count()
{
    return m_count;
}

bool shared_counter::owner_before(const shared_counter& other)
{
    return std::less<manager*>()(m_manager, other.m_manager);
}

bool shared_counter::unique()
{
    return use_count() == 1;
}

void * shared_counter::get_deleter()
{
    return m_manager->get_deleter();
}

void * shared_counter::get_pointer()
{
    return m_manager->get_pointer();
}

void shared_counter::destroy()
{
    if (m_manager) {
        m_manager->destroy();
    }
}

shared_counter::~shared_counter()
{
    if (m_manager) {
        delete m_manager;
        m_manager = nullptr;
    }
}

#endif //TEST_SHARED_COUNTER_H
