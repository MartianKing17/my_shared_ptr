//
// Created by mokovalevskyi on 12/1/20.
//

#define FUNC_NAME(func) #func
#include "test.h"
#include "../include/shared_ptr.h"
#include "Person.h"
#include <algorithm>
#include <random>
#include <sstream>
#include <map>

using namespace memory;

template <class T>
bool check(T value) {
    return value;
}

template<class T, class... Args>
bool check(T value, Args&&... args)
{
    bool res = check(std::forward<Args>(args)...);
    res = value || res;
    return res;
}

void testInit(std::map<std::string, bool>  &result)
{
    result.insert({FUNC_NAME(test_1), test_1()});
    result.insert({FUNC_NAME(test_2), test_2()});
    result.insert({FUNC_NAME(test_3), test_3()});
    result.insert({FUNC_NAME(test_4), test_4()});
    result.insert({FUNC_NAME(test_5), test_5()});
    result.insert({FUNC_NAME(test_6), test_6()});
}

bool test_1()
{
    const size_t size = 5;
    shared_ptr<Person> person;
    shared_ptr<Person> vipPerson1(nullptr);
    shared_ptr<shared_ptr<Person>> arrOfPerson(new shared_ptr<Person>[size], std::default_delete<memory::shared_ptr<Person>[]>());
    person = make_shared<Person>(1, 100, "A", "100");
    vipPerson1 = make_shared<VipPerson>(2, 200, "B", "200", "200");
    int priority{1}, id{100};
    std::string name{'A'}, number{}, cardNumber;

    auto it = arrOfPerson.get();
    std::default_random_engine generator;
    std::uniform_real_distribution<double> dis(0, 1);
    double rand{};

    for (int i = 0; i < size; ++i) {
        rand = dis(generator);
        number = std::to_string(id);
        if (rand < 0.5) {
            it[i] = memory::make_shared<Person>(priority, id, name, number);
        } else {
            cardNumber = number;
            it[i] = memory::make_shared<VipPerson>(priority, id, name, number, cardNumber);
        }
        ++priority;
        id += 100;
        ++name.at(0);
    }

    if (check(!bool(person), !bool(vipPerson1), !bool(arrOfPerson))) {
        return false;
    }

    if (check(!person.unique(), !vipPerson1.unique(), !arrOfPerson.unique())) {
        return false;
    }

    shared_ptr<Person> copyPerson(person);
    shared_ptr<Person> copyVipPerson = vipPerson1;
    shared_ptr<shared_ptr<Person>> copyArrOfPersons = arrOfPerson;
    shared_ptr<Person> vipPerson2(new VipPerson{});

    if (check(person.use_count() != 2, vipPerson1.use_count() != 2, arrOfPerson.use_count() != 2, !vipPerson2.unique())) {
        return false;
    }

    shared_ptr<Person> sOtherCopyUser;
    sOtherCopyUser = vipPerson2;
    vipPerson2 = vipPerson1;

    if (check(!sOtherCopyUser.unique(), vipPerson2.use_count() != 3)) {
        return false;
    }

    if (check(sOtherCopyUser.get() == vipPerson2.get(), vipPerson2.get() != copyVipPerson.get())) {
        return false;
    }

    sOtherCopyUser.reset(new Person{1, 100, "A", "100"});

    if (!sOtherCopyUser.unique()) {
        return false;
    }

    sOtherCopyUser.reset();

    if (sOtherCopyUser.get()) {
        return false;
    }

    return true;
}

bool test_2()
{
    VipPerson * vip = new VipPerson{2, 200, "B", "200", "200"};
    std::unique_ptr<Person, std::default_delete<Person>> uniquePtr(vip, std::default_delete<Person>());
    shared_ptr<Person> personFromUnique(std::move(uniquePtr));

    if (check(!personFromUnique.unique(), uniquePtr.get(), !personFromUnique.get())) {
        return false;
    }

    weak_ptr<Person> weakPerson(personFromUnique);
    shared_ptr<Person> personFromWeak(weakPerson);

    if (check(weakPerson.use_count() != 2, personFromWeak.use_count() != 2, personFromUnique.use_count() != 2)) {
        return false;
    }

    shared_ptr<Person> person(std::move(personFromWeak));

    if (person.get() != vip) {
        return false;
    }

    std::string name("C"), number("300");
    person->set_priority(3);
    person->set_id(300);
    person->set_name(name);
    person->set_number(number);

    if (person->get_priority() != vip->get_priority()) {
        return false;
    }

    shared_ptr<Person> anotherPerson(new Person{1, 100, "A", "100"});
    Person ptrPerson = *anotherPerson;
    person.swap(anotherPerson);

    if (check(person->get_id() != ptrPerson.get_id(), anotherPerson->get_id() != vip->get_id())) {
        return false;
    }

    return true;
}

bool test_3()
{
    const size_t size  = 5;
    size_t sizeForArrPerson{};
    int * pInt = new int[size];

    std::unique_ptr<User, std::default_delete<User>> uniquePtr(new VipPerson{1, 100, "A", "100", "100"}, std::default_delete<User>());
    shared_ptr<VipPerson> vipPerson;
    vipPerson = std::move(uniquePtr);
    shared_ptr<Person> person(vipPerson);

    if (check(uniquePtr.get() != nullptr, vipPerson->get_id() != 100, person->get_id() != 100)) {
        return false;
    }

    memory::shared_ptr<double> dPointer(new double[size]{}, [](double * d) {delete[] d;}, std::allocator<double>());
    memory::shared_ptr<int> iPointer(nullptr, [](int * pointer) { delete[] pointer;}, std::allocator<int>());
    std::for_each(dPointer.get(), dPointer.get() + size, [value = 1.2] (double  &d) mutable {d = value; value += 1.2;});
    std::for_each(dPointer.get(), dPointer.get() + size, [pointer = pInt] (double value) mutable {*pointer = static_cast<int>(value); pointer++;});
    iPointer.reset(pInt);

    if (check(!bool(iPointer), *iPointer != 1)) {
        return false;
    }

    std::for_each(iPointer.get(), iPointer.get() + size, [&sizeForArrPerson](int value) {sizeForArrPerson = std::max(static_cast<int>(sizeForArrPerson), value);});
    shared_ptr<shared_ptr<User>> arrPerson = memory::make_shared<shared_ptr<User>>(sizeForArrPerson);

    int priority{1}, id{100};
    std::string name{'A'}, number{}, cardNumber;

    auto it = arrPerson.get();
    std::default_random_engine generator;
    std::uniform_real_distribution<double> dis(0, 1);
    double rand{};

    for (int i = 0; i < sizeForArrPerson; ++i) {
        rand = dis(generator);

        if (rand < 0.3) {
            it[i] = memory::make_shared<User>(id, name);
        } else {
            number = std::to_string(id);
            if (rand < 0.5) {
                it[i] = memory::make_shared<Person>(priority, id, name, number);
            } else {
                cardNumber = number;
                it[i] = memory::make_shared<VipPerson>(priority, id, name, number, cardNumber);
            }
        }

        ++priority;
        id += 100;
        ++name.at(0);
    }

    bool res = true;
    std::for_each(arrPerson.get(), arrPerson.get() + sizeForArrPerson, [&res](const shared_ptr<User> & it) {

        if (check(it.unique() != 1, it.get() == nullptr)) {
            res = false;
        }
    });

    if (!res) {
        return res;
    }

    return true;
}

bool test_4()
{
    weak_ptr<User> weakPtr1;
    shared_ptr<User> person1(weakPtr1);
    shared_ptr<Person> person2 = memory::make_shared<Person>(1, 100, "A", "100");
    weak_ptr<Person> weakPtr2(person2);
    weak_ptr<User> weakPtr3(weakPtr1);

    if (check(weakPtr1.use_count() != person1.use_count(), weakPtr2.use_count() != person2.use_count())) {
        return false;
    }

    weakPtr1 = person2;
    weakPtr3 = weakPtr1;

    if (check(weakPtr1.lock()->get_id() != weakPtr2.lock()->get_id(),
              weakPtr3.lock()->get_id() != weakPtr2.lock()->get_id())) {
        return false;
    }

    shared_ptr<VipPerson> vipPerson = memory::make_shared<VipPerson>(2, 200, "B", "200", "200");
    weak_ptr<VipPerson> weakPtr4 = vipPerson;
    weakPtr2 = weakPtr4;

    if (weakPtr2.lock()->get_id() != weakPtr4.lock()->get_id()) {
        return false;
    }

    weakPtr1.reset();
    weakPtr3.swap(weakPtr1);

    if (check(weakPtr1.lock().get() == nullptr, weakPtr3.lock().get() != nullptr)) {
        return false;
    }

    return true;
}

bool test_5()
{
    memory::shared_ptr<Person> user = memory::make_shared<Person>(1, 100, "A", "100");
    memory::shared_ptr<User> person = memory::make_shared<Person>(2, 200, "B", "200");
    memory::shared_ptr<User> test = memory::make_shared<Person>(3, 300, "C", "300");
    memory::shared_ptr<User> copyUser(user);
    shared_ptr<User> user2;
    weak_ptr<User> weakPtr = person;

    if (check(!user.owner_before(person), !copyUser.owner_before(person), !user.owner_before(weakPtr))) {
        return false;
    }

    if (check(user == person, user != copyUser, user > person, person < user)) {
        return false;
    }

    if (check(user == nullptr, nullptr == user, user2 != nullptr, nullptr != user2)) {
        return false;
    }

    if (check(nullptr > user, user2 < nullptr, user >= person, user <= nullptr, nullptr >= person)) {
        return false;
    }

    std::ostringstream buf;
    buf << user;
    std::string str1 = buf.str();
    buf.clear(std::ios_base::iostate::_S_eofbit);
    buf << user.get();
    if (buf.str() != str1) {
        return false;
    }

    user2 = static_pointer_cast<User>(person);
    copyUser = dynamic_pointer_cast<Person>(user2);

    if (user2->get_id() != person->get_id()) {
        return false;
    }

    if (check(!bool(copyUser), copyUser->get_id() != person->get_id())) {
        return false;
    }

    if (person.use_count() != 3) {
        return false;
    }

    return true;
}

bool test_6()
{
    shared_ptr<User> user = make_shared<Person>(1, 100, "A", "100");
    shared_ptr<const User> constUser;
    constUser = const_pointer_cast<const User>(user);

    if (constUser->get_id() != user->get_id()) {
        return false;
    }

    User * uniquePerson = new Person{};
    dynamic_cast<Person*>(uniquePerson)->operator=(static_cast<Person>(*dynamic_cast<Person*>(user.get())));
    std::default_delete<User> uniqueDeleter = get_deleter<std::default_delete<User>, User>(user);
    std::unique_ptr<User, std::default_delete<User>> uniquePtr(uniquePerson, uniqueDeleter);

    if (uniquePtr.get()->get_id() != uniquePerson->get_id()) {
        return false;
    }

    uniquePerson = nullptr;

    if (uniquePtr.get()->get_id() != user->get_id()) {
        return false;
    }

    const unsigned short constShortArr[] = {62340, 5550, 200, 500, 2500, 800};
    const size_t size = 3;
    shared_ptr<unsigned int> intArr = make_shared<unsigned int>(size);
    unsigned int * pInt = intArr.get();
    pInt[0] = 363'787'140;
    pInt[1] = 32'768'200;
    pInt[2] = 52'431'300;

    shared_ptr<unsigned short> shortArr = reinterpret_pointer_cast<unsigned short>(intArr);

    for (int i = 0; i < sizeof(constShortArr) / sizeof(short); ++i) {
        if (constShortArr[i] != *(shortArr.get() + i)) {
            return false;
        }
    }

    return true;
}
