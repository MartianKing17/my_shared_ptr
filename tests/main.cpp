#include "test.h"
#include <iostream>
#include <string>

using namespace std;

int main()
{
    map<string, bool> result{};
    testInit(result);
    string str{};

    for (const auto &[key, value] : result)
    {
        str = value ? "\u001b[32m[Passed] \u001b[0m" : "\u001b[31m[Failed] \u001b[0m";
        cout << str + key << endl;
    }
}
