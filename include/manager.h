//
// Created by mokovalevskyi on 1/12/21.
//

#ifndef TEST_MANAGER_H
#define TEST_MANAGER_H
#include <memory>


template <class T>
class pointer
{
public:
    pointer() : m_obj() {}
    T * getPointer()
    {
        return static_cast<T*>(&m_obj);
    }
private:
    T m_obj;
};

class manager
{
public:
    manager() = default;
    virtual void * get_pointer() = 0;
    virtual void * get_deleter() = 0;
    virtual void destroy() { return; };
    virtual ~manager() {}
};


template <class T>
class pointer_manager : public manager
{
public:
    pointer_manager(T * obj = nullptr) : m_obj(obj) {}
    void destroy() override
    {
        delete m_obj;
    }

    void * get_pointer() override
    {
        return m_obj;
    }

    void * get_deleter() override
    {
        return nullptr;
    }

    ~pointer_manager()
    {
        destroy();
    }
private:
    T *  m_obj;
};

template <class T, class Allocator>
class allocate_pointer_manager : public manager
{
public:
    allocate_pointer_manager() : m_obj() {}

    template <class... Args>
    allocate_pointer_manager(Allocator allocator, Args&&... args) : manager()
    {
        std::allocator_traits<Allocator> alloc;
        alloc.construct(allocator, m_obj.getPointer(), std::forward<Args>(args)...);
    }

    void destroy() override
    {
        return;
    }

    void * get_deleter() override
    {
        return nullptr;
    }

    void * get_pointer() override
    {
        return m_obj.getPointer();
    }
    ~allocate_pointer_manager() = default;
private:
    pointer<T> m_obj;
};

template<class T, class Deleter, class Allocator>
class super_manager : public manager
{
public:
    super_manager(T * obj, Deleter deleter, Allocator allocator)
            : m_obj(obj), m_deleter(std::move(deleter)), m_allocator(std::move(allocator)) {}

    void destroy() override
    {
        m_deleter(m_obj);
    }

    void * get_deleter() override
    {
        return static_cast<void*>(&m_deleter);
    }

    void * get_pointer() override
    {
        return static_cast<void*>(m_obj);
    }

    ~super_manager()
    {
        destroy();
    }
private:
    T * m_obj;
    Deleter m_deleter;
    Allocator m_allocator;
};

#endif //TEST_MANAGER_H
