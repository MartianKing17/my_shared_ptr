//
// Created by mokovalevskyi on 12/1/20.
//

#ifndef PRACTICE_TEST_H
#define PRACTICE_TEST_H
#include <map>
#include <string>

void testInit(std::map<std::string, bool> &result);
bool test_1();
bool test_2();
bool test_3();
bool test_4();
bool test_5();
bool test_6();
#endif //PRACTICE_TEST_H
